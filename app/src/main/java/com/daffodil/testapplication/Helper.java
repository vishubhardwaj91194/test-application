package com.daffodil.testapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daffodil.testapplication.listeners.LoginResponceListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Helper {
    public static boolean checkLoggedIn(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.shared_preference_file_name), Context.MODE_PRIVATE);
        return sharedPref.getBoolean(context.getString(R.string.login_key),false);
    }

    public static void setLoggedIn(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.shared_preference_file_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(context.getString(R.string.login_key), true);
        editor.commit();
    }

    public static void setLoggedOut(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.shared_preference_file_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(context.getString(R.string.login_key), false);
        editor.commit();
    }

    public static int getUserId(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.shared_preference_file_name), Context.MODE_PRIVATE);
        return sharedPref.getInt(context.getString(R.string.user_id_key),0);
    }

    public static void setUserId(Context context, int passedUserId){
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.shared_preference_file_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(context.getString(R.string.user_id_key), passedUserId);
        editor.commit();
    }

    public static void redirectToHome(Context context){
        Intent redirect = new Intent(context, HomeActivity.class);
        redirect.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(redirect);
    }

    public static void redirectToLogin(Context context){
        Intent redirect = new Intent(context, MainActivity.class);
        redirect.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(redirect);
    }

    public static void tryLogin(final Context context, final LoginResponceListener listener, final String u, final String p){
        final ProgressDialog progressDialog = getProgressDialog(context.getString(R.string.login_msg_1), context);
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://sites-api.16mb.com/api/api-1/login_user.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        listener.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        listener.onFailure();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(context.getString(R.string.login_key_1),u);
                params.put(context.getString(R.string.login_key_2),p);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public static ProgressDialog getProgressDialog(String message, Context context){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        return progressDialog;
    }

    public static boolean isSuccessfulLogin(String json, Context context){
        boolean isSuccessful = false;
        try {
            JSONObject rootNode = new JSONObject(json);
            JSONObject successArray = rootNode.optJSONObject(context.getString(R.string.login_key_3));
            isSuccessful = successArray.optBoolean(context.getString(R.string.login_key_5));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isSuccessful;
    }

    public static int parseUseIdFromJson(String json, Context context){
        int id = 0;
        try {
            JSONObject rootNode = new JSONObject(json);
            JSONObject successArray = rootNode.optJSONObject(context.getString(R.string.login_key_3));
            id = successArray.optInt(context.getString(R.string.user_id_key),0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }

    public static ArrayList<String> getLoginErrorList(String json, Context context){
        ArrayList<String> failureResponce = new ArrayList<>();
        try {
            JSONObject rootNode = new JSONObject(json);
            JSONArray failureArray = rootNode.optJSONArray(context.getString(R.string.login_key_4));
            for(int i=0;i<failureArray.length();i++){
                failureResponce.add(failureArray.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return failureResponce;
    }

    public static void sessionChecks(Context context){
        if(checkLoggedIn(context)){
            // Logged in
        }else{
            // Not logged in
            redirectToLogin(context);
        }
    }
}

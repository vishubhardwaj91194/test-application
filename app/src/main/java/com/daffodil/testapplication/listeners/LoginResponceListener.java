package com.daffodil.testapplication.listeners;

public interface LoginResponceListener {
    void onSuccess(String responce);
    void onFailure();
}

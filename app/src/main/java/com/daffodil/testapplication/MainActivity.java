package com.daffodil.testapplication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewStub;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daffodil.testapplication.listeners.LoginResponceListener;

public class MainActivity extends AppCompatActivity {
    private ViewStub stub;
    private boolean flag=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(stub==null){
            stub = (ViewStub) findViewById(R.id.visibleLayout);
        }else{
            flag = false;
        }
        start();
    }

    private void start() {

        if(checkNetExistes()){
            // We have internet access
            if(Helper.checkLoggedIn(this)){
                // Already logged in
                // redirect to home page
                Helper.redirectToHome(this);
            }else{
                // Not logged in
                // display login or register button
                if(flag){
                    stub.setLayoutResource(R.layout.signup_signin_layout);
                    View inflated = stub.inflate();
                }
            }

        }
    }

    private boolean checkNetExistes() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            // display error
            Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
            RelativeLayout netNotAvailableLayout = (RelativeLayout) findViewById(R.id.no_net);
            if (netNotAvailableLayout != null) {
                netNotAvailableLayout.setVisibility(View.VISIBLE);
            }
            return false;
        }
    }

    public void changeSignup(View view) {
        RelativeLayout root = (RelativeLayout)findViewById(R.id.yes_net);
        View.inflate(this, R.layout.signup_layout, root);
    }
    public void changeSignin(View view) {
        RelativeLayout root = (RelativeLayout)findViewById(R.id.yes_net);
        View.inflate(this, R.layout.signin_layout, root);
    }
    public void tryLogin(View v){
        String username = ((EditText)findViewById(R.id.editText)).getText().toString();
        String password = ((EditText)findViewById(R.id.editText2)).getText().toString();
        final TextView log = ((TextView)findViewById(R.id.textView6));
        LoginResponceListener listener = new LoginResponceListener() {
            @Override
            public void onSuccess(String responce) {
                if(Helper.isSuccessfulLogin(responce, MainActivity.this)){
                    // Login was successful
                    // redirect
                    // log.setText("suuascessssss.............");
                    //Helper.setLoggedIn(MainActivity.this);
                    Helper.setLoggedIn(MainActivity.this);
                    Helper.setUserId(MainActivity.this,Helper.parseUseIdFromJson(responce, MainActivity.this));
                    Helper.redirectToHome(MainActivity.this);
                } else{
                    // Login failed
                    // display errors
                    String totalResponce = "Folloing errors occured, please resolve:\n\n";
                    for(String error:Helper.getLoginErrorList(responce, MainActivity.this)){
                        totalResponce+=" * "+error+"\n";
                    }
                    log.setText(totalResponce);
                }
            }

            @Override
            public void onFailure() {
                log.setText(getString(R.string.login_msg_2));
            }
        };
        Helper.tryLogin(this, listener, username, password);
    }

}
